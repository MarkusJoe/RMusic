package cn.rtast.rmusic.models.netease.detail

data class M(
    val br: Int,
    val fid: Int,
    val size: Int,
    val sr: Int,
    val vd: Int
)