package cn.rtast.rmusic.models.netease.login

data class AvatarDetail(
    val identityIconUrl: String,
    val identityLevel: Int,
    val userType: Int
)