package cn.rtast.rmusic.models.netease.detail

data class L(
    val br: Int,
    val fid: Int,
    val size: Int,
    val sr: Int,
    val vd: Int
)