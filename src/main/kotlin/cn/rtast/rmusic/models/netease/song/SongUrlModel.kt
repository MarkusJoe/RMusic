package cn.rtast.rmusic.models.netease.song

data class SongUrlModel(
    val code: Int,
    val `data`: List<Data>
)