package cn.rtast.rmusic.models.netease.detail

data class Ar(
    val alias: List<Any>,
    val id: Int,
    val name: String,
    val tns: List<Any>
)