package cn.rtast.rmusic.models.netease.search

data class SearchRespModel(
    val code: Int,
    val result: Result
)