package cn.rtast.rmusic.models.cookie

data class CookieModel(
    val cookies: List<Cookie>
)