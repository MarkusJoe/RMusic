package cn.rtast.rmusic.models.cookie

data class Cookie(
    val cookie: String,
    val platform: String
)